#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2012 Rodolphe Quiedeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from os import path, access, X_OK
from os import chdir
from sys import argv
from commands import getoutput
from optparse import OptionParser

VERSION = "1.2.0"


def trigger_start(string):
    """
    Return true if we can begin to count
    """
    if string.startswith('language,filename,blank,comment,code'):
        return True
    else:
        return False


def trigger_stop(string):
    """
    Return true if we have to stop counting
    """
    if string.startswith('files,language,blank,comment,code'):
        return True
    else:
        return False


def lang(string):
    """
    Return then language name formatted
    """
    langname = string.lower()
    if langname == 'bourne shell':
        langname = 'shell'
    return langname


def namedir(string):
    """
    Return the name dir "a la sloccount"
    """
    if string.startswith('/'):
        nmd = path.dirname(string).split('/')[1]
    else:
        nmd = path.dirname(string).split('/')[0]

    if nmd == '.':
        nmd = 'top_dir'
    return nmd


def load_exclude(filename):
    """
    Look if an exlude file is present
    """
    optname = '--exclude-list-file'
    if path.isfile(filename):
        return '%s=%s' % (optname, path.abspath(filename))
    else:
        return ""


def readopts(cmdargs):
    """
    Read options passed on command line
    """
    opts = ""
    parser = OptionParser()
    parser.add_option("--exclude-list-file",
                      action="store",
                      type="string",
                      dest="exclude_filelist",
                      default=None)

    parser.add_option("--binary",
                      action="store",
                      type="string",
                      dest="clocpath",
                      default="/usr/bin/cloc")

    options = parser.parse_args(args=cmdargs)[0]

    if options.exclude_filelist is not None:
        opts = load_exclude(options.exclude_filelist)

    if options.clocpath is not None:
        if not path.isfile(options.clocpath):
            exit('File does not exists : %s' % (options.clocpath))
        if not access(options.clocpath, X_OK):
            exit('File does not exists : %s' % (options.clocpath))
    return options.clocpath, opts


def cloc_cmdline(fpath, cmdarg):
    """
    Build the cloc command line
    """
    (binary, cloc_opts) = readopts(cmdarg)
    cmdline = "%s --csv %s --by-file-by-lang %s %s""" % (binary,
                                                         '--exclude-dir=.git',
                                                         cloc_opts,
                                                         fpath)
    return cmdline


def parse_cloc(text):
    """
    Parse the cloc output
    """
    flag = False
    output = ""
    for line in text.split('\n'):
        if trigger_stop(line):
            flag = False

        if flag:
            datas = line.split(',')
            output += '%s\t%s\t%s\t%s\n' % (datas[4],
                                            lang(datas[0]),
                                            namedir(datas[1]),
                                            datas[1])

        if trigger_start(line):
            flag = True
    return output


def main(arguments):
    """
    Main function
    """
    fpath = arguments[len(arguments) - 1]
    if path.isdir(fpath):
        chdir(fpath)
        fpath = '.'
    cloc = cloc_cmdline(fpath, arguments[1:])
    text = getoutput(cloc)
    return parse_cloc(text)


if __name__ == '__main__':    # pragma: no cover
    print main(argv)
