#! /usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2012 Rodolphe Quiedeville <rodolphe@quiedeville.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from tempfile import mkstemp, mkdtemp
import sys
import stat
import syslog
import urllib2
sys.path.append('../')

import unittest

from clokins import *


class Testsclokins(unittest.TestCase):

    def test_trigger_start(self):

        self.assertEqual(trigger_start('foobar'), False)
        self.assertEqual(trigger_start('language,filename,blank,comment,code'), True)
        self.assertEqual(trigger_start('language,filename,blank,comment,code,foobar'), True)

    def test_trigger_stop(self):

        self.assertEqual(trigger_stop('foobar'), False)
        self.assertEqual(trigger_stop('files,language,blank,comment,code'), True)
        self.assertEqual(trigger_stop('files,language,blank,comment,code,foobar'), True)

    def test_lang(self):

        self.assertEqual(lang('Foobar'), 'foobar')
        self.assertEqual(lang('bourne shell'), 'shell')
        self.assertEqual(lang('Bourne Shell'), 'shell')

    def test_namedir(self):

        self.assertEqual(namedir('/foobar/bar'), 'foobar')
        self.assertEqual(namedir('./foobar/bar'), 'top_dir')
        self.assertEqual(namedir('./'), 'top_dir')

    def test_readopts_empty(self):

        self.assertEqual(readopts([]), ('/usr/bin/cloc', ''))

    def test_readopts(self):
        """
        The exclude file exists
        """
        tmp = mkstemp()[1]
        options = ["--exclude-list-file=%s" % (tmp)]
        attend = ('/usr/bin/cloc', "--exclude-list-file=%s" % tmp)
        self.assertEqual(readopts(options), attend)

    def test_readopts_bin(self):
        """
        Read options with valid binary
        """
        tmp = mkstemp()[1]
        exe = mkstemp()[1]
        os.chmod(exe, stat.S_IXUSR)
        options = ["--exclude-list-file=%s" % (tmp),
                   "--binary=%s" % (exe)]
        attend = (exe, "--exclude-list-file=%s" % (tmp))
        self.assertEqual(readopts(options), attend)

    def test_readopts_bin(self):
        """
        Read options with binary not executable
        """
        tmp = mkstemp()[1]
        exe = mkstemp()[1]
        options = ["--exclude-list-file=%s" % (tmp),
                   "--binary=%s" % (exe)]
        attend = (exe, "--exclude-list-file=%s" % (tmp))
        #self.assertEqual(readopts(options), attend)
        self.assertRaises(SystemExit, readopts, options)

    def test_readopts_bindne(self):
        """
        Read options with binary that does not exists
        """
        tmp = mkstemp()[1]
        exe = '/foo/bar'
        options = ["--exclude-list-file=%s" % (tmp),
                   "--binary=%s" % (exe)]
        attend = (exe, "--exclude-list-file=%s" % (tmp))
        #self.assertEqual(readopts(options), attend)
        self.assertRaises(SystemExit, readopts, options)

    def test_readopts_notexists(self):
        """
        The exclude file does not exists
        """
        tmp = mkstemp()[1]
        os.remove(tmp)
        options = ["--exclude-list-file=%s" % (tmp)]
        self.assertEqual(readopts(options), ('/usr/bin/cloc', ''))

    def test_cloc_cmdline(self):
        """
        The cloc commande line
        """
        result = cloc_cmdline('/foobar', [])
        attend = "/usr/bin/cloc --csv --exclude-dir=.git --by-file-by-lang  /foobar"
        self.assertEqual(result, attend)


    def test_parse_cloc(self):
        """
        The parser
        """
        text = 'foo\n'
        text = text + 'language,filename,blank,comment,code\n'
        text = text + 'Javascript,./js/jquery-1.7.2.min.js,0,1,3\n'
        text = text + 'files,language,blank,comment,code\n'
        attend = '3\tjavascript\ttop_dir\t./js/jquery-1.7.2.min.js\n'
        result = parse_cloc(text)
        self.assertEqual(result, attend)

    def test_parsecloc_two(self):
        """
        The parser with two lines
        """
        text = 'foo\n'
        text = text + 'language,filename,blank,comment,code\n'
        text = text + 'Javascript,./js/jquery-1.7.2.min.js,0,1,3\n'
        text = text + 'Javascript,./js/openlayers-2.10.min.js,0,1,3\n'
        text = text + 'files,language,blank,comment,code\n'
        attend = '3\tjavascript\ttop_dir\t./js/jquery-1.7.2.min.js\n'
        attend += '3\tjavascript\ttop_dir\t./js/openlayers-2.10.min.js\n'
        result = parse_cloc(text)
        self.assertEqual(result, attend)


    def test_main_file(self):
        """
        The main sub
        """
        tmp = mkstemp()[1]
        fmp = open(tmp, 'w')
        fmp.write('foobar')
        fmp.close()
        args = [tmp]
        self.assertEqual(main(args), '')

    def test_main_dir(self):
        """
        The main sub with a directory as option
        """
        tmp = mkdtemp()
        #  Create a file in temp dir
        fmp = open(path.join(tmp, 'foobar.py'), 'w')
        fmp.write('foobar')
        fmp.close()
        args = [tmp]
        attend = '1\tpython\ttop_dir\t./foobar.py\n'
        self.assertEqual(main(args), attend)


suite = unittest.TestLoader().loadTestsFromTestCase(Testsclokins)
unittest.TextTestRunner(verbosity=2).run(suite)
